from flask import Flask
from flask import jsonify,request
import os
import socket
import time
import ipaddress
import urllib
import random
import string

app = Flask(__name__)



def _randomString_(stringLength=10):
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(stringLength))


def _is_open_(ip, port):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.settimeout(2)
    try:
        s.connect((ip, int(port)))
        s.shutdown(2)
        return True
    except:
        return False

def _add_ip_allow_hosts(ip):
    _clear_all_allowed_ips_()
    with open('/etc/hosts.allow','a') as hosts:
        hosts.write("sshd : {}\n".format(ip))


def _clear_all_allowed_ips_():
    lines = []
    with open('/etc/hosts.allow', 'r') as hosts:
        lines = hosts.readlines()
        for line in lines:
            if line.startswith('sshd'):
                lines.remove(line)
    with open('/etc/hosts.allow', 'w') as hosts:
        hosts.writelines(lines)

def _deny_all_ssh_():
    lines = []
    with open('/etc/hosts.deny', 'r') as hosts:
        lines = hosts.readlines()
        for line in lines:
            if line.startswith('sshd'):
                lines.remove(line)
        lines.append("sshd : ALL\n")
    
    with open('/etc/hosts.deny', 'w') as hosts:
        hosts.writelines(lines)

def _flip_nginx_access_config_():
    lines = []
    new_lines=[]
    with open('/etc/nginx/nginx.conf', 'r') as nginx_conf:
        lines = nginx_conf.readlines()

    for line in lines:
        if "default yes" in line or "default no" in line:
            if "yes" in line:
                new_lines.append(line.replace("yes","no"))
            else:
                new_lines.append(line.replace("no","yes"))
        else:
            new_lines.append(line)

    with open("/etc/nginx/nginx.conf","w") as nginx_conf:
        nginx_conf.writelines(new_lines)

def __restart_nginx__() -> str:
    return os.system("supervisorctl restart nginx")


@app.route("/lockout", methods=["GET"])
def lockout():
    _deny_all_ssh_()
    _clear_all_allowed_ips_()
    return "LOCKED DOWN",200

@app.route("/allowme", methods=["GET"])
def allowme():
    _add_ip_allow_hosts(request.remote_addr)
    return jsonify({'ip-address': request.remote_addr}), 200


@app.route("/checkme", methods=["GET"])
def checkme():
    res = []
    for port in [22,25,80,443]:
        if _is_open_('google.com', port):
            res.append(port)
    
    return jsonify({'open_ports':res}),200

@app.route("/log")
def log():
    with open('logs.csv','a+') as logs:
        logs.write("'{}','{}','{}'".format(time.ctime(), request.remote_addr, request.headers.get("User-Agent")))

    return "'{}','{}','{}'".format(time.ctime(), request.remote_addr, request.headers.get("User-Agent")), 200


@app.route("/checkAvailability", methods=["GET"])
def checkAvailability():
    if 'url' not in request.args:
        return "",400

    url = request.args.get('url')
    ip = socket.gethostbyname(url)
    if not ipaddress.ip_address(ip).is_private:
        if urllib.request.urlopen("http://"+ip).getcode() == 200:
            return "Website Available", 200
   
    return "Website Blocked", 200


@app.route("/IRANAccess", methods=["GET"])
def IRANAccess():
    _flip_nginx_access_config_()
    result = __restart_nginx__()
    return str(result),200


@app.route("/dnsCheck",methods=["GET"])
def dnsCheck():
    if 'sub' not in request.args: 
        subdomain = _randomString_(7)
        with open("subdomains.txt","w") as sub_file:
            sub_file.write("{}\n".format(subdomain))
        return
    """
    <html>
    <body>

    <h2>Probing DNS with {0}</h2>
    <img src="{0}.saeidbahmanisangesari.ir/img.png" onerror="myFunction()"\>
    
    <script>
        function myFunction() {{
        location.replace("http://45.77.161.37/dnsCheck?sub={0}")
        }}
    </script>

    </body>
    </html>
    """.format(subdomain)
    else:
        sub = request.args.get('sub')
        with open("dnsleaks.txt","r") as dnsleaks:
            leaks = dnsleaks.readlines()
        open("dnsleaks.txt","w")
        return jsonify(dns=leaks)
        



@app.route("/")
def hello():
    return "<h1 style='color:red'>Hello you!</h1>"

if __name__ == "__main__":
    app.run(host='0.0.0.0')
    
